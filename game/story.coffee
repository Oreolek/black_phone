# This room has no description.
# The description is constructed from the objects within.
room "living-room", salet,
  title: "Living room"
  enter: (character, system, from) ->
    if (from == "start")
      audio = document.getElementById("bgsound")
      audio.currentTime=0
      audio.volume = 0.5
      audio.play()
  ways: ["bedroom", "kitchen", "balcony"]
  objects: [
    obj "window",
      act: """
        The moon is full today.
        It illuminates the apartment, makes the things stand out in some weird angles.
      """
      order: 0
      dsc: "Ronald is standing in a dark room with a big {{window}}."
    obj "walls",
      dsc: "{{The walls}} are covered with a dingy rose wallpaper."
      act: """
        There are colorful photographs on the walls.
        A wooden house in a forest.
        A village on a mountaintop.
        A family sitting around a fire.
        A sunset burning in a vast ocean.
        A black monolith standing on sand dunes.
      """
    obj "door",
      dsc: "Oh, and {{the door Ronald came into}} the apartment is there, too."
      order: 99
      act: (salet) ->
        if salet.character.box_opened == 0
          return "Ronald has a job here. It's still on."
        else
          return salet.goTo("exitdoor")
    obj "zane",
      visible: false,
      act: """
        A three-part romantic urban fantasy detective about a girl who solves murders while dining
        with her best werewolf friend in an Orient café.

        It looks rather new, the pages are still white and straight.
        Maybe she didn't catch the right moment to read this.. or maybe just forgot about it.
      """
    obj "magic",
      visible: false,
      act: """
        ”Soviet Black Magic: a Lost Art”

        The author, who calls himself The Master, describes the lost art of Soviet Union *magija.*
        It's mostly about illusions.

        This is a cheap paperback edition, but she read this at least a couple of times.
        The pages have dogged ears, and coffee stains too.
        Ronald can even see a hint of lipstick smearing the variety show retelling.
      """
    obj "dictionary",
      visible: false,
      act: """
        A big fat Dictionary of Everything, issued in 1989.
        Nobody reads every page of these.
        Ronald doubts Anastasia got to read at least one page.
      """
    obj "silent",
      visible: false,
      act: """
        Silent Things, a story where nothing speaks and nothing happens.
        *And she actually read that.*
      """
    obj "zaratustra",
      visible: false,
      act: (salet) ->
        if salet.character.seen_zaratustra?
          return """
            This book already gave Ronald everything he wanted.
            No need to read it, not a bit.
          """
        else
          money(salet, 20000)
          return """
            Nietsche's four-part novel about The Man, The Superman and everything in-between.
            It's surprisingly worn down.
            She took this book out a lot.

            Was she secretly a philosophy nut?
            An Übermensch dreamer?

            No, of course not.
            Ronald opens the book and finds a stash of money inside.
          """
    obj "pepper",
      visible: false,
      act: """
        An "ironic woman detective" who plays harp and solves murders!
        It's a trash book filled with blatant product placement. Looks untouched.
      """
    obj "culinary",
      visible: false,
      act: "An old culinary book. Nothing about it."
    obj "classico",
      visible: false,
      act: """
        A history of classical music, from the prehistoric times to the 1970s, a brand new edition with passages about psychedelic rock.
        The Bach section is bookmarked.
      """
    obj "tv",
      dsc: "#{objlink("A book stand", "bookcase")} is hanging above {{a television set.}}"
      order: 1
      act: """
        An expensive 40-something inch TV standing on a stylish black stand. The room looks kinda small for that monster.
      """
    obj "bookcase",
      order: 0
      act: """
        Either Anastasia has a very conflicting taste in books, or she has no taste at all. Let's see...
        #{objlink("“Soviet Black Magic: a Lost Art”,", "magic")}
        #{objlink("”My Dinner with Zane”,", "zane")}
        #{objlink("The Scientific Dictionary of Everything,", "dictionary")}
        #{objlink("“Silent Things”,", "silent")}
        #{objlink("”Also sprach Zarathustra”,", "zaratustra")}
        #{objlink("”Pepperoni poker”,", "pepper")}
        #{objlink("”Jazz sauce. Culinary collection”", "culinary")}
        and #{objlink("Classico.", "classico")}
      """
  ]

room "bedroom", salet,
  title: "Bedroom"
  ways: ["living-room", "kitchen", "bathroom"]
  after: (salet, from) ->
    if from == "bathroom"
      return "Ronald doesn't want to search the bathroom. It's too private a room to enter."
  dsc: (salet) ->
    return """
      The bedroom is spacious; its walls are lavender green, almost white in the moonlight.

      On the wall across #{objlink("a big bed", "bed")} hangs #{objlink("a full sized mirror.", "mirror")}

      #{if salet.character.box_opened == 0
        "On a small table near the bed is an ornate #{way_to("wooden box.", "box")}"
      else ""}
    """
  objects: [
    obj "bed",
      dsc: false,
      order: 2
      act: """
        A double bed with flower-embroidered sheets.
        She left several days ago.
        The sheets are still fresh.
      """
    obj "wardrobe",
      dsc: "{{A massive wardrobe}} occupies one of the walls.",
      order: 1,
      act: """
        Anastasia's wardrobe is very high-maintenance.
        It has a built-in ironing board (with an iron hanged nearby), with 5 drawer rows for #{objlink("lingerie,", "lingerie")} #{objlink("accessories", "accessories")}, #{objlink("shoes.", "shoes")}, #{objlink("hats", "hats")} and.. #{objlink("audio players.", "mp3")}

        On the hangers are #{objlink("cashmere coat,", "coat")} #{objlink("sport jacket,","jacket")} #{objlink("jeans,", "jeans")} #{objlink("green shirt,", "gshirt")} #{objlink("a red sleeveless shirt", "rshirt")}, #{objlink("an orange vest,", "vest")} #{objlink("knee-length flower dress,", "dress")} #{objlink("another flower dress,", "adress")} #{objlink("alpaca coat,","coat")} #{objlink("a short skirt", "skirt")} and #{objlink("a big collection of dancing costumes.", "costumes")}
      """
    obj "coat",
      visible: false
      act: "A warm coat for the cold winter."
    obj "jacket",
      visible: false,
      act: """
        A light expensive jacket.
        An unusual material, must be something high-tech.
        It's slightly used.
      """
    obj "jeans",
      visible: false,
      act: "Just a pair of women jeans, nothing special."
    obj "gshirt",
      visible: false,
      act: """
        A green shirt, looks very worn.
        It's not remarkable in any way but maybe she just loves it very much.
      """
    obj "rshirt",
      visible: false,
      act: """
        A red women-cut shirt.
        She didn't wear it much.
      """
    obj "vest",
      visible: false,
      act: """
        It looks like a life vest but actually it's a fashionable piece of warm clothing.
        It was trendy last year.
        *Why do I know this, is something wrong with me?*
      """
    obj "dress",
      visible: false,
      act: """
        Just a dress.
        Lots of flower embroidery, no pockets.
        *Impractical.*
      """
    obj "adress",
      visible: false,
      act: "These flowers are not like that flowers."
    obj "coat",
      visible: false,
      act: (salet) ->
        if salet.character.seen_coat?
          return """
            A warm grey alpaca coat for the bleak autumn times.
            It's one of her favorites.
          """
        else
          salet.character.seen_coat = 1
          money(salet, 4000)
          return """
            A warm coat.. hey, what's this?
            One of the pockets is loaded with cash!
          """
    obj "skirt",
      visible: false,
      act: """
        This hanger has only a short skirt.
        Maybe there was something else on it?
        Who knows.
      """
    obj "costumes",
      visible: false,
      act: """
        Ana is an exotic dancer.
        She has her own dance style, and these exquisite costumes are made just for her moves and motions.
      """
    obj "mp3",
      visible: false,
      act: """
        Wow, this woman LOVES her players!

        There are MP3 players, CD players, portable DVD, walk-on clips, sport hands-free players, underwater ones.

        These are all rather cheap, though, compared to *something else* in this room.
      """
    obj "hats",
      visible: false,
      act: """
        These look very old-style, very Mary Poppins-like.
        Maybe that's just a trend or whatever.
      """
    obj "lingerie",
      visible: false,
      act: "Ronald won't be digging in that."
    obj "accessories",
      visible: false,
      act: """
        A cross necklace, three metal bracelets and lots of uncomplicated earrings and hair pins.
        A dozen of scarfs or so.
        No diamonds, no rings, no *jewelry.*

        On the other hand, her Instagram nickname is *bareboned mane shaker.*
      """
    obj "shoes",
      visible: false,
      act: """
        Anastasia doesn't care for the footwear fashion.
        These 4 pairs of combat boots and 13 pairs of ballet flats can attest that.

        Of course, there are sandals, loafers, flat dress shoes.. That's a strong dislike for heels... or maybe it's a medical problem?
      """
    obj "mirror",
      order: 3
      dsc: false
      act: """
        The mirror looks directly at the bed.
        Kinky, though not very much *Feng Shui* in it.

        #{objlink("The frame","frame")} depicts various artists (all women) making sculptures of men.
        It's a very *unusual* art.
      """
    obj "frame",
      visible: false,
      act: """
        On a close examination, the frame isn't attached to the wall.
        There is #{objlink("a safe", "safe")} behind the mirror!
      """
    obj "safe",
      visible: false
      act: (salet) ->
        if salet.character.seen_safe?
          return """
            The safe is locked with a regular lock.
            Ronald tries two keys.
            The first of them opens the door.

            There are #{objlink("money", "money")} inside, and #{objlink("a rough sketch.", "sketch")}
          """
        else
          return """
            The safe is open now.

            There is #{objlink("a rough sketch", "sketch")} inside.
          """
    obj "money",
      visible: false
      act: (salet) ->
        salet.character.seen_safe = 1
        money(salet, 50000)
        return """
          It's a big cash pile.
          Odd that she didn't take this when she left.
          But someone's fault just makes Ronald's payday now.
        """
    obj "sketch",
      visible: false
      act: """
        It's a portrait of Anastasia.
        She bites her lower lip slightly.
        Her eyes are sad, or maybe concerned with something.
        The sketch is signed: *"L. Y. - 2017"*
      """
  ]

room "kitchen", salet,
  title: "Kitchen"
  ways: ["living-room", "bedroom"]
  dsc: """
    The white, perfectly clean kitchen could be called spartan: #{objlink("a fridge,", "fridge")} a microwave
    and #{objlink("a big table", "table")} where one can eat whatever she "cooked" that way.
  """
  objects: [
    obj "fridge",
      dsc: ""
      act: """
        No magnets or stickers on the outside.
        The door opens easily.
        *If only the hinges on the apartment doors were as good as refrigerator ones.*

        A hearty bunch of salad.

        Some fruits, carrots, two beets.

        Three eggs, one cracked.

        A bottle of ketchup, bottle of whiskey, valerian vial.

        A jar of raspberry preserve, half-finished.

        And enough frozen pizzas to last a month.
      """
    obj "table",
      dsc: ""
      act: (salet) ->
        if salet.character.seen_table?
          return "A letter's still there. Nothing new about it."
        else
          salet.character.seen_table = 1
          return """
            There's something on the table.

            It looks like a formal letter.
            It's in French, though, so Ronald won't be able to read it.
            He's sure it's recent (`24.03.2018`) and it's about something-something QUANTUM AUDIO.. armement?
          """
  ]

room "bathroom", salet,
  enter: (salet) ->
    bedroom = salet.getRoom("bedroom")
    index = bedroom.ways.indexOf("bathroom")
    bedroom.ways.splice(index, 1)
    return salet.goTo("bedroom")
  title: "Bathroom"
  ways: ["bedroom"]

room "balcony", salet,
  title: "Balcony"
  ways: ["living-room"]
  dsc: """
    A small glazed-in empty balcony.
    It's an amazing night.
    The whole town is lit by moonlight, standing perfectly still.

    On a short stand is #{objlink("an ashtray","ashtray")} with some ash in it.
  """
  objects: [
    obj "ashtray",
      dsc: false,
      act: (salet) ->
        salet.character.knows_the_code = 1
        return """
          She completely smoked out two cigarettes here.
          There's also a #{objlink("piece of paper nearby,", "paper")} half-burnt.
        """
    obj "paper",
      visible: false,
      act: """
        It's a letter, written by hand on a thick sheet of what must be an A4 paper.
        The handwriting is wobbly and the first three quarters of the sheet is gone, but the ending is legible.

        *...ok at them, celebrating the New Year, think of our anniversary.
        The day of White.
        I will fly to you no matter what.*

        &nbsp;&nbsp;&nbsp; *L. Y.*
      """
  ]

room "box", salet,
  ways: ["bedroom"]
  choices: "#box"
  dsc: (salet) ->
    return """
      It's a red wood, very expensive.
      And this box is locked with a digital code key.
      #{if salet.isVisited(this.name) == 0
        """
          Ronald takes out a vial from his pocket. He coats the keys with a bright white powder.

          Only 1, 2, 7 and 0 keys are fingerprinted.

          He wipes the box clean until there is no trace of the powder.
        """
      }
    """

room "smash", salet,
  canView: (salet) ->
    salet.character.view_smash == 1
  optionText: "Smash the box"
  before: (salet) ->
    salet.character.view_smash = 0
  choices: "#box"
  tags: ["box"]
  dsc: "Ronald still needs the phone in this box. A very high-tech fragile phone. Smashing isn't an option."

safe_button = (number, salet) ->
  room "put#{number}", salet,
    choices: "#box"
    tags: ["box"]
    optionText: "Enter #{number}"
    before: (salet) ->
      code_input(salet, number)
    canChoose: (salet) ->
      code_can_input(salet)
    dsc: (salet) -> """
      Ronald presses button #{number}. The display is #{code_print(salet)} now.
    """
    after: (salet) ->
      code_check(salet)

safe_button(1, salet)
safe_button(2, salet)
safe_button(7, salet)
safe_button(0, salet)

room "reset", salet,
  choices: "#box"
  tags: ["box"]
  optionText: "Reset the display"
  dsc: (salet) ->
    code_reset(salet)
    """
      Ronald presses Backspace until the display is empty.
    """

room "postpone", salet,
  tags: ["box"]
  optionText: "Continue looking around the apartment"
  enter: (salet) ->
    salet.goTo('bedroom')

room "exitdoor", salet,
  ways: ["living-room"]
  choices: "#door"
  dsc: """
    Ronald is ready to go.
    Maybe he's satisfied with his explorations or just wants to finish this.
    But then a new problem arrives.

    Someone's shadow is under the doorframe.
  """

room "finale", salet,
  before: () ->
    _paq.push(['setCustomDimension', 1, true])
    $("#tools_wrapper").hide()
  optionText: "Use the Phone"
  tags: ["door"]
  ways: []
  dsc: (salet) -> """
    "LOADING... 100%"

    Ronald opens the door and presses his finger to the phone screen.

    ### A CACOPHONY OF MONKEY SOUNDS

    There stands a tall man in a grey jacket and plaid shirt.
    *Another Instagram follower, perhaps?*

    An older man in his pajamas and slippers lies motionless on the floor near him.

    ### A QUANTUM MAGICAL SMART PHONE FIESTA

    The man in a grey jacket slumps as if struck by something.
    Ronald taps the Phone again and quietly escapes the floor.

    “Well, that was a good night.”

    #{if salet.character.money > 0
      "The pocket is heavy with #{salet.character.money} rubles and the phone."
    else
      "The phone is heavy in the pocket."
    }
    The sun is coming up soon.

    If you're not sleeping at night, you risk learning something secret about your neighbors.

    <center><h3>END</h3></center>
  """
