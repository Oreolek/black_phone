# copyright (c) Alexander Yakovlev 2015.
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
# To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0

room = require("../../lib/room.coffee")
obj = require('../../lib/obj.coffee')
oneOf = require('../../lib/oneOf.coffee')
Salet = require('../../lib/salet.coffee')

salet = new Salet
salet.game_id = "6a9909a4-586a-4089-bd18-26da684d1c8d"
salet.game_version = "2.0"
salet.init = () ->
  _paq.push(['setCustomDimension', 1, false])

  @character.view_smash = 1
  @character.money = 0
  @character.code = ""
  @character.knows_the_code = 0
  @character.box_opened = 0
$(document).ready(() ->
  salet.beginGame()
)

way_to = (content, ref) ->
  return "<a href='#{ref}' class='way'>#{content}</a>"
objlink = (content, ref) ->
  return "<a href='./_act_#{ref}' class='once'>#{content}</a>"

money = (salet, amount) ->
  salet.character.money = salet.character.money + amount

code_can_input = (salet) ->
  return salet.character.code.length < 8 and salet.character.box_opened != 1

code_print = (salet) ->
  mask = 8 - salet.character.code.length
  retval = salet.character.code
  if mask > 0
    for i in [1..mask]
      retval += 'x'
  retval = '<tt>'+retval+'</tt>'
  return retval

code_input = (salet, digit) ->
  if code_can_input(salet)
    salet.character.code = salet.character.code + digit
  return false

code_reset = (salet) ->
  salet.character.code = ""

code_check = (salet) ->
  output = ""
  if salet.character.code.length >= 8
    # There is an Undum.. let's call it a feature
    # that prevents the player from entering "3112".
    # You see, you can't select the situation 1 when you are
    # already in this situation, so you can't input 1 twice.
    if salet.character.code == "01012017"
      salet.character.box_opened = 1
      if salet.character.knows_the_code == 0
        output += """
          Is he an extraordinary puzzle cracker or was it a sheer luck, but Ronald manages to *guess* the code.
        """
      else
        output += """
          New Year 2017.
          L. Y. must be Leonard Yakovlev, a famous painter.
          Some tabloids tried to connect him with Ana but it seemed like a weak link.

          By that logic, his sketch is worth more than all the cash here.
          Ronald thinks about it, but decides to "let the woman have her memories".
        """
      output += """
        \n\n
        Something clicks and box opens.

        The phone is slick, black and light in Ronald's hand.
        It springs to life, humming with purpose.
        The screen plays an animation: "LOADING..."

        Ronald has no other business here.
        It's time to go.
      """
    else
      output += "Something clicks and the display resets, but the box stays locked."

    code_reset(salet)
    return output

room "start", salet,
  extendSection: true,
  dsc: """
    Peter opened his door to find an empty silent corridor.
    He went to the neighbor's door and met a closed door.
    Ronald was working inside, quietly walking around the apartment.
    He began the inspection from #{way_to('the living room.', 'living-room')}
  """

# Remove every section marked as a different level.
# For a link level 0, we hide every link of level 1 and above.
# It's for the player to focus.
changeLevel: (level) =>
  maxLevel = 6
  if level < maxLevel
    i = level + 1
    hideArray = []
    while i <= maxLevel
      hideArray.push("#content .lvl"+i)
      i++
    directive = hideArray.join(", ")
    $(directive).hide()

wrapLevel: (text, level) =>
  return "<div class='lvl#{level}'>"+markdown(text)+'</div>'
