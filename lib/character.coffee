class Character
  constructor: (spec) ->
    @inventory = []

    @take = (thing) =>
      @inventory.push thing
    @drop = (thing) =>
      for i in @inventory
        if i.name == thing
          index = @objects.indexOf(thing)
          @inventory.splice(index, 1)
    
    for index, value of spec
      this[index] = value
    return this

character = (spec) ->
  spec ?= {}
  return( new Character(spec) )

module.exports = character
