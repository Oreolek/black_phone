room = require("../room.coffee")
randomid = require("./randomid.coffee")
###
A phrase shortcut.
Usage:

    phrase "Point out a thing in her purse (mildly)", "start", """
        Point out a thing in her purse (mildly)
      """, "character.sandbox.mild = true"

@param title phrase Phrase (question)
@param salet Salet core
@param string tag tag marking viewing condition
@param string text Response
@param string effect an optional parameter, eval'd code
###
phrase = (title, salet, tag, text, effect) ->
  retval = room(randomid(), salet, {
    optionText: title
    dsc: text
    clear: false # backlog is useful in dialogues
    choices: "#"+tag
    tags: [tag]
  })
  if effect?
    retval.before = (character, system) ->
      eval(effect)
  return retval

module.exports = phrase
