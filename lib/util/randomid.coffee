module.exports = () ->
  alphabet = "abcdefghijklmnopqrstuvwxyz0123456789" # see the dreaded linkRe expression in Undum
  rndstr = []
  for i in [1..10]
    rndstr.push alphabet.charAt(Math.floor(Math.random() * alphabet.length))
  return rndstr.join('').toString()
